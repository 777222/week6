package week6.domain;

import java.util.ArrayList;

public class Train implements Car, Locomotive {
    private String name;
    private String type;
    private int carNumb;
    private int passengers;
    private static final double capacity = 1.5;
    private static final int maxPassengers = 20;

    public Train(String name, String locomotiveType, int carNumb, int passengers)
    {

        setName(name);
        setLocomotiveType(locomotiveType);
        setCarNumb(carNumb);
        setPassengers(passengers);
    }

    private void setName(String name) {
        this.name = name;
    }

    @Override
    public void setLocomotiveType(String type) {
        this.type = type;
    }

    @Override
    public String getLocType() {
        return type;
    }

    @Override
    public void setCarNumb(int n) {
        this.carNumb = n;
    }

    @Override
    public double getCapacity() {
        return carNumb * 1000;


    }

    @Override
    public void setPassengers(int n) {
        if(maxPassengers * carNumb >= n)
        {
            this.passengers = n;
        }
        else
        {
            System.out.println("Too many passengers in train!!!");
        }
    }

    @Override
    public int getPassengers() {
        return passengers;
    }

    @Override
    public String toString()
    {
        return name + "\n" + "Locomotive type: " + getLocType() + "\nCar number: " + carNumb + "\nPassenger number: " + getPassengers() + "\n \n";
    }
}

